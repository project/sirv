<?php

/**
 * @file
 * Functions related to Sirv image fields.
 */

/**
 * Prepares variables for Sirv image templates.
 *
 * Default template: sirv-image.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - url: A URL to an image hosted by Sirv.
 *   - alt: The alternative text for text-based browsers.
 *   - title: The title text is displayed when the image is hovered in some
 *     popular browsers.
 *   - attributes: Associative array of attributes to be placed in the img tag.
 *   - sirv_settings: An associate array of settings specific to Sirv images.
 *     - profile: A Sirv profile to use for processing the image.
 *     - extra_options: Sirv ptions that will override options from the
 *       profile. Example of format: w=250&grayscale=true'.
 *     - responsive: A boolean indicating whether the image will be displayed
 *       using Sirv's responsive behavior.
 *     - responsive_scale_crop: An option for scaling and cropping an image to
 *       fit a container.
 *     - responsive_resize_step: the change in browser width required for a new
 *       responsive image to be generated.
 *     - lazy_load: A boolean indicating whether the image will be displayed
 *       using Sirv's lazy load behavior.
 *     - lazy_load_threshold: a threshold value to indicate when to lazy load
 *       the image before it comes into view.
 */
function template_preprocess_sirv_image(array &$variables) {
  // Get the site-wide Sirv settings.
  $sirv_settings = \Drupal::config('sirv.settings')->get();

  // Get the image-specific Sirv settings.
  $sirv_image_settings = $variables['sirv_settings'];

  // Set defaults for any missing Sirv field settings.
  $sirv_image_settings += [
    'profile' => NULL,
    'responsive' => FALSE,
    'responsive_scale_crop' => NULL,
    'responsive_resize_step' => NULL,
    'lazy_load' => FALSE,
    'lazy_load_threshold' => NULL,
  ];

  $attributes = $variables['attributes'];

  // Define an empty array for the data-options attribute.
  $data_options = [];

  // Make sure the "class" attribute exists.
  if (!isset($attributes['class'])) {
    $attributes['class'] = [];
  }

  $responsive = $sirv_image_settings['responsive'];
  $lazy_load = $sirv_image_settings['lazy_load'];

  if (!empty($sirv_settings['domain'])) {
    // Add header links to preconnect and prefetch DNS.
    $variables['#attached']['html_head_link'][][] = [
      'rel' => 'preconnect',
      'href' => 'https://' . $sirv_settings['domain'],
      'crossorigin' => TRUE,
    ];
    $variables['#attached']['html_head_link'][][] = [
      'rel' => 'dns-prefetch',
      'href' => 'https://' . $sirv_settings['domain'],
    ];
  }

  if ($responsive || $lazy_load) {
    // Set the data-src attribute to the Sirv URL and remove the src attribute.
    if (!empty($variables['url'])) {
      $attributes['data-src'] = $variables['url'];
      unset($attributes['url']);
    }

    $attributes['class'][] = 'Sirv';

    // Add the Sirv library.
    $variables['#attached']['library'][] = 'sirv/sirv';

    // Add header links to preconnect and prefetch DNS.
    $variables['#attached']['html_head_link'][][] = [
      'rel' => 'preconnect',
      'href' => SIRV_SCRIPTS_DOMAIN,
      'crossorigin' => TRUE,
    ];
    $variables['#attached']['html_head_link'][][] = [
      'rel' => 'dns-prefetch',
      'href' => SIRV_SCRIPTS_DOMAIN,
    ];
  }
  else {
    if (!empty($variables['url'])) {
      $attributes['src'] = $variables['url'];
    }
  }

  if ($responsive) {
    if (!$lazy_load) {
      $data_options['lazy'] = 'false';
    }

    // Handle the Responsive Scale/Crop setting.
    switch ($sirv_image_settings['responsive_scale_crop']) {
      case 'contain':
        $attributes['class'][] = 'image-fit';
        break;

      case 'cover':
        $attributes['class'][] = 'image-fill';
        $data_options['fit'] = 'cover';
        break;

      case 'crop':
        $attributes['class'][] = 'image-fill';
        $data_options['fit'] = 'crop';
        break;
    }

    // Handle the Responsive Resize Step setting.
    if ($sirv_image_settings['responsive_resize_step']) {
      $data_options['resizeStep'] = $sirv_image_settings['responsive_resize_step'];
    }
  }

  if ($lazy_load) {
    if (!$responsive) {
      $data_options['resize'] = 'false';
    }

    // Handle the Lazy Load Threshold setting.
    if ($sirv_image_settings['lazy_load_threshold']) {
      $data_options['threshold'] = $sirv_image_settings['lazy_load_threshold'];
    }
  }

  // Compile the data options into a data-options attribute.
  $data_options_attribute = NULL;
  $data_options_items = [];
  foreach ($data_options as $key => $value) {
    $data_options_items[] = "$key:$value";
  }
  $data_options_attribute = implode(';', $data_options_items);

  if ($data_options_attribute) {
    $attributes['data-options'] = $data_options_attribute;
  }

  $variables['attributes'] = $attributes;

  foreach (['alt', 'title'] as $key) {
    if (isset($variables[$key])) {
      // If the property has already been defined in the attributes,
      // do not override, including NULL.
      if (array_key_exists($key, $variables['attributes'])) {
        continue;
      }
      $variables['attributes'][$key] = $variables[$key];
    }
  }
}
