<?php

namespace Drupal\sirv\Plugin\Field\FieldFormatter;

use Drupal\sirv\SirvUrlInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'sirv_image' formatter.
 *
 * @FieldFormatter(
 *   id = "sirv_image",
 *   label = @Translation("Sirv image"),
 *   field_types = {
 *     "image"
 *   },
 *   weight = 10
 * )
 */
class SirvImageFormatter extends ImageFormatter {

  /**
   * The Sirv settings.
   *
   * @var array
   */
  protected $sirvSettings;

  /**
   * The Sirv URL service.
   *
   * @var \Drupal\sirv\SirvUrlInterface
   */
  protected $sirvUrlService;

  /**
   * Constructs a SirvImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\sirv\SirvUrlInterface $sirv_url_service
   *   The Sirv URL service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $image_style_storage, ConfigFactoryInterface $config_factory, SirvUrlInterface $sirv_url_service) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $current_user, $image_style_storage);

    $this->sirvSettings = $config_factory->get('sirv.settings')->get();
    $this->sirvUrlService = $sirv_url_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('config.factory'),
      $container->get('sirv.url')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'profile' => '',
      'extra_options' => '',
      'responsive' => TRUE,
      'responsive_scale_crop' => '',
      'responsive_resize_step' => '',
      'lazy_load' => TRUE,
      'lazy_load_threshold' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['profile'] = [
      '#title' => $this->t('Sirv profile'),
      '#type' => 'textfield',
      '#description' => $this->t('Enter a Sirv profile to use for processing images in this field.'),
      '#default_value' => $this->getSetting('profile'),
    ];
    $element['extra_options'] = [
      '#title' => $this->t('Extra Sirv options'),
      '#type' => 'textfield',
      '#description' => $this->t('These options will override options from the profile. Example of format: %example', ['%example' => 'w=250&grayscale=true']),
      '#default_value' => $this->getSetting('extra_options'),
    ];

    $element['responsive'] = [
      '#title' => $this->t('Make responsive'),
      '#type' => 'checkbox',
      '#description' => $this->t('Check to enable responsive behavior for this field.'),
      '#default_value' => $this->getSetting('responsive'),
    ];
    $element['responsive_scale_crop'] = [
      '#title' => $this->t('Automatic scale/crop'),
      '#type' => 'select',
      '#description' => $this->t('Select an option for scaling and cropping an image to fit a container.'),
      '#default_value' => $this->getSetting('responsive_scale_crop'),
      '#options' => [
        'contain' => $this->t('Contain (default)'),
        'cover' => $this->t('Cover'),
        'crop' => $this->t('Crop'),
      ],
    ];
    $element['responsive_resize_step'] = [
      '#title' => $this->t('Resize step'),
      '#type' => 'number',
      '#description' => $this->t('Enter the change in browser width required for a new responsive image to be generated.'),
      '#default_value' => $this->getSetting('responsive_resize_step'),
      '#size' => 10,
      '#field_suffix' => $this->t('px'),
    ];

    $element['lazy_load'] = [
      '#title' => $this->t('Use lazy loading'),
      '#type' => 'checkbox',
      '#description' => $this->t('Check to enable lazy loading for this field.'),
      '#default_value' => $this->getSetting('lazy_load'),
    ];
    $element['lazy_load_threshold'] = [
      '#title' => $this->t('Threshold'),
      '#type' => 'textfield',
      '#description' => $this->t('Enter a threshold value, in either number of pixels or percentage, to indicate when to lazy load the image before it comes into view. (Examples: 200, 50%)'),
      '#default_value' => $this->getSetting('lazy_load_threshold'),
      '#size' => 10,
    ];

    $image_styles = image_style_options(FALSE);
    $element['image_style'] = [
      '#title' => $this->t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $this->t('The original image is usually desired, though an image style may be selected if it provides an effect that is not available in Sirv, such as custom cropping.'),
    ];

    $link_types = [
      'content' => $this->t('Content'),
      'file' => $this->t('File'),
    ];
    $element['image_link'] = [
      '#title' => $this->t('Link image to'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_link'),
      '#empty_option' => $this->t('Nothing'),
      '#options' => $link_types,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if (!empty($this->getSetting('profile'))) {
      $summary[] = $this->t('Sirv profile: @profile', ['@profile' => $this->getSetting('profile')]);
    }

    if (!empty($this->getSetting('extra_options'))) {
      $summary[] = $this->t('Extra Sirv options');
    }

    if (!empty($this->getSetting('responsive'))) {
      $summary[] = $this->t('Responsive');
    }

    if (!empty($this->getSetting('lazy_load'))) {
      $summary[] = $this->t('Lazy load');
    }

    if (!empty($this->getSetting('image_style'))) {
      $summary[] = $this->t('Image style: @style', ['@style' => $this->getSetting('image_style')]);
    }

    $link_types = [
      'content' => $this->t('Linked to content'),
      'file' => $this->t('Linked to file'),
    ];
    $image_link_setting = $this->getSetting('image_link');
    if (isset($link_types[$image_link_setting])) {
      $summary[] = $link_types[$image_link_setting];
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Get the parent's elements.
    $elements = parent::viewElements($items, $langcode);

    // Process each element.
    foreach ($elements as &$element) {
      $element = $this->processElement($element);
    }

    return $elements;
  }

  /**
   * Generate the Sirv URL and add Sirv settings.
   *
   * @param array $element
   *   The element to be processed.
   *
   * @return array
   *   The processed element.
   */
  protected function processElement(array $element) {
    $item = $element['#item'];

    if (!$file = $item->entity) {
      return $element;
    }

    // Get the image-specific settings.
    $settings = $this->getSettings();

    // Generate the Sirv URL.
    $sirv_url = $this->sirvUrlService->generateFromFile($file, $settings);

    // If the image is linked to the file, use the Sirv URL.
    if ($this->getSetting('image_link') == 'file') {
      $element['#url'] = Url::fromUri($sirv_url);
    }

    // Remove settings not specific to Sirv.
    unset($settings['image_style']);
    unset($settings['image_link']);

    // Add the Sirv data to the element.
    $element['#sirv'] = [
      'type' => 'image',
      'url' => $sirv_url,
      'settings' => $settings,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // Get the site-wide Sirv settings.
    $sirv_settings = \Drupal::config('sirv.settings')->get();

    // This formatter should not be available if the Sirv domain is not defined.
    return !empty($sirv_settings['domain']) && parent::isApplicable($field_definition);
  }

}
