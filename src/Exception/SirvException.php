<?php

namespace Drupal\sirv\Exception;

/**
 * Class used to differentiate Sirv exceptions from others.
 */
class SirvException extends \Exception {}
