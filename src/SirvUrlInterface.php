<?php

namespace Drupal\sirv;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an interface for a Sirv URL service.
 */
interface SirvUrlInterface {

  /**
   * Generate and return a Sirv URL from a file entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $file
   *   The file entity.
   * @param array $sirv_image_settings
   *   The image-specific Sirv settings.
   *
   * @return string
   *   The Sirv URL.
   */
  public function generateFromFile(EntityInterface $file, array $sirv_image_settings = []);

}
