<?php

namespace Drupal\sirv\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form to enter Sirv settings.
 */
class SirvSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sirv_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sirv.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('sirv.settings');

    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#description' => $this->t('The domain used to serve images from Sirv. (Example: example.com)'),
      '#default_value' => $config->get('domain'),
      '#required' => TRUE,
      '#field_prefix' => 'https://',
    ];
    $form['directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Directory'),
      '#description' => $this->t('An optional subdirectory within your Sirv account where files will be stored. Do not include leading or trailing slashes.'),
      '#default_value' => $config->get('directory'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('sirv.settings')
      ->set('domain', $values['domain'])
      ->set('directory', trim($values['directory'], "/ \t\n\r\0\x0B"))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
