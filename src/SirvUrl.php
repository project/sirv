<?php

namespace Drupal\sirv;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\image\Entity\ImageStyle;

/**
 * Defines a Sirv URL service.
 */
class SirvUrl implements SirvUrlInterface {

  /**
   * The Sirv settings.
   *
   * @var array
   */
  protected $sirvSettings;

  /**
   * Constructs a new Sirv URL service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->sirvSettings = $config_factory->get('sirv.settings')->get();
  }

  /**
   * {@inheritdoc}
   */
  public function generateFromFile(EntityInterface $file, array $sirv_image_settings = []) {
    $sirv_settings = $this->sirvSettings;

    // Determine the domain to use.
    $domain = $sirv_settings['domain'];

    // Get the original image's URI.
    $file_uri = $file->getFileUri();

    // If an image style was selected, build the URI to the file.
    if (!empty($sirv_image_settings['image_style'])) {
      $image_style = ImageStyle::load($sirv_image_settings['image_style']);
      $file_uri = $image_style->buildUri($file_uri);
    }

    // Get the URI without its scheme.
    $file_uri_target = \Drupal::service('stream_wrapper_manager')->getTarget($file_uri);

    // Create the Sirv URL.
    $sirv_url = "https://$domain";

    // Add a directory, if required.
    if (!empty($sirv_settings['directory'])) {
      $sirv_url .= '/' . $sirv_settings['directory'];
    }

    // Add the file URI target.
    $sirv_url .= '/' . $file_uri_target;

    // Add a profile and options, if required.
    $sirv_url = $this->addQueryParams($sirv_url, $sirv_image_settings['profile'], $sirv_image_settings['extra_options']);

    return $sirv_url;
  }

  /**
   * Add query parameters (profile and options) to a URL.
   *
   * @param string $url
   *   The URL to which query parameters should be added.
   * @param string $profile
   *   The name of the Sirv profile to add.
   * @param string $options
   *   The Sirv options to be added.
   *
   * @return string
   *   The modified URL.
   */
  protected function addQueryParams($url, $profile = NULL, $options = NULL) {
    // Parse the URL into parts.
    $url_parts = UrlHelper::parse($url);

    // No non-Sirv parameters are allowed.
    $url_parts['query'] = [];

    // Add the profile.
    if ($profile) {
      $url_parts['query']['profile'] = $profile;
    }

    // Add the options.
    if ($options) {
      $options_array = [];
      $option_items = explode('&', $options);
      foreach ($option_items as $option_item) {
        $option = explode('=', $option_item);
        if (!empty($option[0]) && isset($option[1])) {
          $options_array[$option[0]] = $option[1];
        }
      }
      $url_parts['query'] += $options_array;
    }

    // Reassemble the URL.
    $url = $url_parts['path'];
    $url .= !empty($url_parts['query']) ? '?' . UrlHelper::buildQuery($url_parts['query']) : '';
    $url .= !empty($url_parts['fragment']) ? '#' . $url_parts['fragment'] : '';

    return $url;
  }

}
