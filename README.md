# Sirv

The Sirv module provides support within Drupal for integration with [Sirv](“https://sirv.com”), an image processing and hosting platform, offering functionality that can replace or augment Drupal’s native image styles and responsive image styles.

## What does this module do?

Sirv can be used in combination with, or as a replacement for, the image style and responsive image style functionality offered by Drupal core. In addition to the default image effects provided by Drupal, such as scaling, cropping, and rotating, Sirv offers sophisticated color adjustment and filtering, along with effects that add vignettes, frames and watermarks.

## Sirv profiles

Sirv’s equivalent to Drupal’s image style is a profile, which is stored in JSON format in a file within your Sirv account. Profiles are packages of effects and options, available to be applied to any image uploaded to Sirv. To output an image with a specific Sirv profile, select the “Sirv Image” field formatter for the image field on the Manage Display form and select a profile in the field formatter options. You may also enter custom options in the key-value format expected by Sirv.

## Responsive images (Drupal 8 only)

To employ Sirv’s powerful responsive image behavior, check the Responsive option for the “Sirv Image” field formatter.

## Lazy loading (Drupal 8 only)

To enable lazy loading for an image, check the Lazy Load option for the “Sirv Image” field formatter.

## Requirements

This module requires a free or paid Sirv account. To create one, go to Sirv.com.

### Drupal 8

The Sirv module for Drupal 8 only requires the Image module provided by Drupal core.

### Drupal 7

The Sirv module for Drupal 7 requires the following:

*   Drupal 7.36 or later
*   Image (Drupal core)
*   S3 File System
*   Amazon S3 SDK for PHP

## Installation

### Drupal 8

The preferred method for adding the Sirv module to your project is to use Composer: `composer require drupal/sirv` Then install it normally using Drush or Drupal’s UI.

### Drupal 7

Install and enable the module in the usual Drupal fashion. In order to upload images to Sirv with a standard Drupal file field, you will need to install the S3 File System module. You will also need to add the following line to your settings.php file:

`$conf['image_suppress_itok_output'] = TRUE;`

In order to make Sirv profiles available within Drupal, enter your connection information (key, secret key, bucket, host, and profiles directory) on Sirv’s main configuration page (/admin/config/media/sirv).

Choose S3 as your upload destination when creating the field. Then select Sirv Image as the format for the field and enter your settings, which include a Sirv profile to use to process the image, along with additional options to apply to that field.

#### Sirv Zoom

Sirv Zoom lets you rapidly zoom into images to see detail. Zoom options can be be combined into profiles within Drupal (/admin/config/media/sirv/zoom-profiles).

To make into a Sirv Zoom any image (or images) uploaded with an image field, select Sirv Zoom as the format for the field and enter your settings, which include a Sirv Zoom profile to use, along with any additional zoom options to apply Sirv profile to apply to that field. You can also select a default Sirv Image profile to use to process the image and additional profiles that can apply at specified breakpoints.

If multiple images are uploaded to the field, they can either be grouped into one Zoom, which enables thumbnails for navigating between images, or each image can be rendered as a separate Zoom.

For best results with Sirv Zoom, upload the highest-resolution version of your image that you have.
